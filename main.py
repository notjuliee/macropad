import asyncio
import evdev

from glob import glob
from toml import loads
from jinja2 import Environment, FileSystemLoader
from os import chdir
from subprocess import Popen
from sys import path

chdir(path[0])

async def handle_system(device, args):
    Popen("(cd; setsid " + args["command"] + "&> /dev/null)", shell=True)


async def handle_led(device, args):
    led = {v:  k for k, v in evdev.ecodes.LED.items()}[args["led"]]
    if args["action"] == "toggle":
        state = False
        for x in device.leds(True):
            if x[0] == args["led"]:
                state = bool(x[1])
                break
        device.set_led(led, int(not state))
    else:
        device.set_led(led, args["action"])


ACTION_HANDLERS = {
    "system": handle_system,
    "led": handle_led
}


async def event_loop(device, config):
    device.grab()
    async for event in device.async_read_loop():
        if event.type != evdev.ecodes.EV_KEY or event.value != 1:
            continue
        print(evdev.ecodes.KEY[event.code])
        for key in config["key"]:
            if key["ecode"] == evdev.ecodes.KEY[event.code]:
                for action in key["action"]:
                    if action["type"] not in ACTION_HANDLERS:
                        print("WARN: " + action["type"] + " is unknown")
                        continue
                    await ACTION_HANDLERS[action["type"]](device, action)


ENV = Environment(loader=FileSystemLoader(["Config", "Include"]), trim_blocks=True)

for f in glob("Config/*.toml"):
    f = f[len("Config/"):]
    t = ENV.get_template(f)
    conf = loads(t.render())
    for p in conf["device"]["paths"]:
        dev = evdev.InputDevice(p)
        asyncio.ensure_future(event_loop(dev, conf))

loop = asyncio.get_event_loop()
loop.run_forever()
